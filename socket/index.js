import * as config from './config';

const connected = new Map();
const progress = new Map();
let isGameStarted = false;
const timers = { beforeTimer: config.SECONDS_TIMER_BEFORE_START_GAME, gameTimer: config.SECONDS_FOR_GAME };
let leaderboard = []

const isAllReady = (obj) => [...obj.values()].every(x => x);
const isEveryDone = (obj) => [...obj.values()].every(x => x === 100);

const calcRemainPlayers = () => {
  const usersList = [...progress.entries()].filter(([name, score]) => !leaderboard.includes(name));
  usersList.sort(([, score1], [score2]) => score2 - score1);
  usersList.forEach(([username]) => leaderboard.push(username));
};


export default io => {
  io.on('connection', socket => {
    const username = socket.handshake.query.username;

    if (isGameStarted) {
      socket.emit('already_on', { message: 'Game already in progress' });
    } else if (connected.has(username) || username === 'null') {
      socket.emit('already_on', { message: 'User already in a game' });
    } else {
      connected.set(username, false);
      progress.set(username, 0);
    }

    io.sockets.emit('send_users', [...connected]);

    socket.on('on_ds', ({ query }) => {
      const { username } = query
      connected.delete(username);
      progress.delete(username);
      io.sockets.emit('send_users', [...connected]);

      if (leaderboard.includes(username)) {
        leaderboard.splice(leaderboard.indexOf(username), 1);
      }

      if ([...connected].length === 0) {
        isGameStarted = false;
      }

      if (isAllReady(connected) && !isGameStarted) {
        io.sockets.emit('start_game', timers);
      }

      if (isEveryDone(progress)) {
        io.sockets.emit('all_finished');
      }
    });

    socket.on('user_ready', (username) => {
      const isReady = connected.get(username);
      connected.set(username, !isReady);
      io.sockets.emit('send_users', [...connected]);

      if (isAllReady(connected)) {
        io.sockets.emit('start_game', timers);
        isGameStarted = true;
      }
    });

    socket.on('user_progress', ({ username, currentProgress }) => {
      if (connected.has(username) && progress.has(username)) {
        progress.set(username, currentProgress);
      };
      io.sockets.emit('send_progress', [...progress]);

      if (currentProgress === 100) {
        leaderboard.push(username);
      }

      if (isEveryDone(progress)) {
        io.sockets.emit('all_finished');
      }
    });

    socket.on('game_end', () => {
      if (isGameStarted) {
        isGameStarted = false;

        calcRemainPlayers();
        io.sockets.emit('final_progress', leaderboard);
        leaderboard = [];
        [...connected.keys()].forEach((name) => {
          connected.set(name, false);
          progress.set(name, 0);
        });
        io.sockets.emit('send_users', [...connected]);
      }
    });
  });
};
