export const texts = {
  0: "Text for typing #1",
  1: "Text for typing #2",
  2: "Text for typing #3",
  3: "Text for typing #4",
  4: "Text for typing #5",
  5: "Text for typing #6",
  6: "Text for typing #7",
  length: 7,

  getRandomText() {
    if (!this.last) {
      const random = Math.floor(Math.random() * Math.floor(this.length));
      this.last = this[random];
      setTimeout(() => { this.last = null }, 10000 /*ms (10s) delay*/);
      return this.last;
    }
    return this.last;
  },
};

export default { texts };
