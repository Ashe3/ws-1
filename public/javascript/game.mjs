import { socket } from './sockets.mjs';
import { hideButtons, renderTimer, removeGameField, showButtons } from './renders.mjs';
import { callApiGet } from './helpers.mjs';
import { toggleReadyBtn, initializeListeners } from './listeners.mjs';

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

export const startGame = ({ beforeTimer, gameTimer }) => {
  hideButtons();
  renderTimer(beforeTimer, game, { gameTimer });
};

const game = async ({ gameTimer }) => {
  const textForTyping = await callApiGet();
  initializeListeners(textForTyping, gameTimer, endGame);
};

export const endGame = () => {
  socket.emit('game_end');
  removeGameField();
  toggleReadyBtn();
  showButtons();
};