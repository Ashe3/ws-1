import { createElement, addClass, removeClass } from './helpers.mjs';

const username = sessionStorage.getItem('username');
const userListElement = document.getElementById('users-list');
const btns = document.getElementsByClassName('game-btn');
const gameField = document.getElementById('text-field');
const mainGameField = document.getElementById('game-field')


const renderUser = ([name, isReady]) => {
  const userElement = createElement({ tagName: 'div', className: 'user', attributes: { id: name } });
  const userReady = createElement({ tagName: 'div', className: 'ready-checkbox' });
  if (isReady) {
    addClass(userReady, 'ready')
  }
  const userNameElement = createElement({ tagName: 'span' });
  userNameElement.innerText = name;
  if (name === username) {
    userNameElement.innerText += '  (you)';
  }

  const progressMain = createElement({ tagName: 'div', className: 'progress-bar empty' });
  const progressBar = createElement({ tagName: 'div', className: 'progress-bar current-progress' });
  const progressEnd = createElement({ tagName: 'div', className: 'progress-bar already-done' });

  userElement.append(userReady, userNameElement, progressMain, progressBar, progressEnd);
  return userElement;
};

export const renderUsersList = (list) => {
  const usersList = list.map(renderUser);
  userListElement.innerHTML = "";
  userListElement.append(...usersList);
};

export const hideButtons = () => {
  [...btns].forEach(btn => {
    addClass(btn, 'display-none');
  });
};

export const showButtons = () => {
  [...btns].forEach(btn => {
    removeClass(btn, 'display-none');
  });
};

export const renderTimer = (time, func, funcData = {}, element = gameField) => {
  let timer = time;

  let timerId = setInterval(() => {
    if (timer <= 0) {
      clearInterval(timerId);
      element.innerText = "";
      func(funcData);
    } else {
      element.innerText = timer;
    }
    timer = timer - 1;
  }, 1000 /*ms delay*/);
};

export const renderGameField = (textForTyping, current, typed) => {
  gameField.innerHTML = "";

  const forTypingElement = createElement({ tagName: 'span', attributes: { id: 'types-left' } });
  forTypingElement.innerText = textForTyping;

  const currentTypeElement = createElement({ tagName: 'span', attributes: { id: 'current-type' } });
  currentTypeElement.innerText = current;

  const typedElement = createElement({ tagName: 'span', attributes: { id: 'typed' } });
  typedElement.innerText = typed;

  gameField.append(typedElement, currentTypeElement, forTypingElement);
};

export const renderGameTimer = (playTime, func) => {
  const element = document.getElementById('game-field')

  const timer = createElement({ tagName: 'div', attributes: { id: 'gameTimer' } });
  const time = createElement({ tagName: 'span', attributes: { id: 'time' } });
  time.innerText = playTime;
  const text = createElement({ tagName: 'span' });
  text.innerText = 'seconds left';

  timer.append(time, text);
  mainGameField.append(timer);

  renderTimer(playTime, func, {}, time);
};

export const renderUsersProgress = (users) => {
  users.forEach(([name, width]) => {
    const player = document.getElementById(name);
    if (width < 100) {
      player.getElementsByClassName('current-progress')[0].style.width = `${width}%`;
    } else {
      player.getElementsByClassName('current-progress')[0].style.opacity = 0;
      player.getElementsByClassName('already-done')[0].style.opacity = 1;
    }
  });
};

export const removeGameField = () => {
  gameField.innerHTML = "";
  const u = document.getElementById('gameTimer');
  if (u) {
    mainGameField.removeChild(u);
  }
};

export function showModal({ title, bodyElement, onClose = () => { } }) {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root.append(modal);
};

function getModalContainer() {
  return document.getElementById('root');
};

function createModal({ title, bodyElement, onClose }) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
};

function createHeader(title, onClose) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
};

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
};

export function showWinnerModal(playersData) {
  const title = 'Results:';
  const bodyElement = createElement({ tagName: 'span', className: 'modal-body', attributes: {} });
  const results = playersData.reduce((acc, name, index) => {
    const nameSpan = createElement({ tagName: 'span' });
    nameSpan.innerText = `#${index + 1} ${name}`;
    return [...acc, nameSpan];
  }, []);

  bodyElement.append(...results);
  showModal({ title, bodyElement, onClose: () => { } });
};
