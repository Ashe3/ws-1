import { socket } from './sockets.mjs';
import { renderGameTimer, renderGameField } from './renders.mjs';

const ds = document.getElementById('ds');
const startBtn = document.getElementById('start-btn');
const username = sessionStorage.getItem('username');

export const initializeListeners = (text, time, func) => {

  let textForTyping = text.split('');
  let currentChar = textForTyping.shift();
  let typedText = [];

  renderGameTimer(time, func);
  renderGameField(textForTyping.join(''), currentChar, typedText.join(''));

  const keyListener = (event) => {
    if (event.key === currentChar) {
      typedText.push(currentChar);
      currentChar = textForTyping.shift();

      if (!currentChar) {
        currentChar = "";
      }

      renderGameField(textForTyping.join(''), currentChar, typedText.join(''));
      const currentProgress = (typedText.length / text.length) * 100; //%
      socket.emit('user_progress', { username, currentProgress });

      if (currentProgress === 100) {
        window.removeEventListener('keydown', keyListener);
      }
    }
  };
  window.addEventListener('keydown', keyListener);
};

startBtn.addEventListener('click', () => {
  socket.emit('user_ready', username);
  toggleText(startBtn, 'Ready', 'Not ready');
});

const toggleText = (obj, text1, text2) => {
  if (obj.innerText === text1) {
    obj.innerText = text2;
  } else {
    obj.innerText = text1;
  }
};

export const disconnectUser = () => {
  socket.emit('on_ds', { query: { username } });
  socket.emit('disconnect');
  sessionStorage.removeItem('username');
  window.location.replace('/login');
};

ds.addEventListener('click', disconnectUser);

export const toggleReadyBtn = () => {
  startBtn.innerText = 'Ready';
};

window.onbeforeunload = () => {
  disconnectUser();
};
