import { startGame, endGame } from './game.mjs';
import { renderUsersList, renderUsersProgress, showWinnerModal } from './renders.mjs';
import { initializeListeners, disconnectUser } from './listeners.mjs';

const username = sessionStorage.getItem('username');

export const socket = io('', { query: { username } });

socket.on('already_on', ({ message }) => {
  disconnectUser()
  alert(message);
});

socket.on('send_users', (connected) => {
  renderUsersList(connected);
});

socket.on('start_game', (timers) => {
  startGame(timers);
});

socket.on('send_progress', (data) => {
  renderUsersProgress(data);
});

socket.on('all_finished', () => {
  endGame();
  window.removeEventListener('keydown', initializeListeners.keyListener)
});

socket.on('final_progress', (data) => {
  showWinnerModal(data);
});
